import { Component, OnInit } from '@angular/core';
import { WoocommerceService} from '../../services/woocommerce.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  categories = ['Abstract Art', 'Sculpture', 'paintings', 'ceramics'];
  constructor(private wooService: WoocommerceService) { }

  ngOnInit(): void {
    this.wooService.getProducts().subscribe(data => {
      console.log(data);
    })
  }

}
