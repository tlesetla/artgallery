import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const header = new HttpHeaders({
  'Content-Type': 'application/x-www-form-urlencoded',
});
@Injectable({
  providedIn: 'root',
})
export class WoocommerceService {
  url = 'https://www.culturestore.co.za/store';
  consumerKey = 'ck_624b9b9c0e511da71b8e3a38f7387c6e09ba4d9a';
  consumerSecret = 'cs_47da2a5ba7a773ef3fcbb69ab554df8dec3e09af';

  constructor(private http: HttpClient) { }

  getProducts(): Observable<any> {
    return this.http.get<any>(`${this.url}/wp-json/wc/v3/products?consumer_key=${this.consumerKey}&consumer_secret=${this.consumerSecret}`,
      { headers: header }
    );
  }
}
